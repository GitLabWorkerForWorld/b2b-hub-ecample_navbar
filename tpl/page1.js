Vue.component('page1', {
    template:
        '<div data-tpl="page1">' +
        '    <input v-model="msg">' +
        '    <button v-on:click="notify">' +
        '    change value to {{msg}}' +
        '    </button>'+
        '</div>',
    props: ['msg'],
    data: function() {
        return { msg: ''}
    },
    methods: {
        'notify': function() {
            console.log('test');
        }
    }
})
