Vue.component('navbar', {
    template:
    '<div data-tpl="navbar" v-bind:class="weui_tab">' +
    '    <div v-bind:class="weui_navbar">' + //v-bind:class
    '       <div v-on:click="switchPage" v-bind:class="[weui_item, weui_item_on]">page1</div>' +
    '       <div v-on:click="switchPage" v-bind:class="[weui_item]">page2</div>' +
    '    </div>' +
    '</div>',
    props: ['child'],
    data: function() {
        return {
            //Set element css class name.
            weui_tab: 'weui_tab',//weui_tab
            weui_navbar: 'weui_navbar',//weui_navbar
            weui_item: 'weui_navbar_item',//weui_item
            weui_item_on: 'weui_bar_item_on'//item_on
        }
    },
    methods: {
        'switchPage': function() {
            //console.log(event.target);
            //Change page view components via user trigger event.
            this.child = event.target.innerText;
            //Use jquery selector to change navbar style(on or off).
            $(event.target).addClass('weui_bar_item_on').
                siblings('.weui_bar_item_on').
                    removeClass('weui_bar_item_on');
        }
    }
})
