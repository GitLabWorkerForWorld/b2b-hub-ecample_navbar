Vue.component('index', {
    template:
        '<div data-tpl="index">' +
        '    <div style="height:100px"><navbar :child.sync=child></navbar></div>' +
        '    <component :is="child" ' +
        '               transition="fade" ' +
        '               transition-mode="out-in" ' +
        '               :msg.sync=msg></compnent>' +
        '</div>',
    data: function() {
        return {
            child: 'page1'
        }
    },
    props: ['msg']
})
