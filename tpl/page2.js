Vue.component('page2', {
    template:
        '<div data-tpl="page2">' +
        '   <input v-model="msg">' +
        '   <p>This content from page1(msg): </p>' +
        '   <p>{{msg}}</p>' +
        '</div>',
    props: ['msg']
})
